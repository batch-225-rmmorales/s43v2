const txtTitle = document.querySelector("#txt-title");
const txtBody = document.querySelector("#txt-body");
const formAddPost = document.querySelector("#form-add-post");
const txtEditTitle = document.querySelector("#txt-edit-title");
const txtEditBody = document.querySelector("#txt-edit-body");
const formEditPost = document.querySelector("#form-edit-post");
const divPost = document.querySelector("#div-post-entries");

let posts = [];
let postsInner;
let editId;

function showPosts() {
  postsInner = "";
  posts.forEach((post) => {
    if (post.isActive) {
      postsInner =
        postsInner +
        `<div class = "post">
      <h2>${post.title}</h2>
      <p>${post.body}</p>
      <p>postId: ${post.id}</p>
      <button onclick="editPost(${post.id})">Edit</button>
      <button onclick="deletePost(${post.id})">Delete</button>
      </div>  
      
      `;
    }
  });
  divPost.innerHTML = postsInner;
}
formAddPost.addEventListener("submit", (event) => {
  event.preventDefault();
  posts.push({
    id: posts.length,
    title: txtTitle.value,
    body: txtBody.value,
    isActive: true,
  });
  txtTitle.value = "";
  txtBody.value = "";
  showPosts();
});

function editPost(postId) {
  txtEditTitle.value = posts[postId].title;
  txtEditBody.value = posts[postId].body;
  editId = postId;
}
function deletePost(postId) {
  posts[postId].isActive = false;
  showPosts();
}

formEditPost.addEventListener("submit", (event) => {
  event.preventDefault();

  posts[editId].title = txtEditTitle.value;
  posts[editId].body = txtEditBody.value;
  txtEditTitle.value = "";
  txtEditBody.value = "";
  editId = undefined;
  showPosts();
});
